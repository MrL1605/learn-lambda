const request = require("request");
const JSSoup = require("jssoup").default;

const getDownloadGramAPIKeys = (callback, errClb) => {
    request.get("https://downloadgram.com",
        (err, resp, body) => {
            if (err) {
                errClb(err);
                return;
            }

            let downloaderHomePage = new JSSoup(body);
            let keyVal = {};
            for (let eachInput of downloaderHomePage.findAll("input")) {
                if (eachInput.attrs["type"] && eachInput.attrs["type"].toLowerCase() === "hidden") {
                    keyVal[eachInput.attrs["name"]] = eachInput.attrs["value"];
                }
            }
            if (keyVal["build_id"] && keyVal["build_key"])
                callback(keyVal["build_id"], keyVal["build_key"]);
            else
                errClb("Could not find value");
        });
};

const getImgLink = (insta_url) => {
    return new Promise((resolve, reject) => {
        getDownloadGramAPIKeys((id, key) => {
            let query = `url=${encodeURIComponent(insta_url)}&build_id=${id}&build_key=${key}`;
            request.post({
                    url: "https://downloadgram.com/process.php",
                    headers: {"Content-Type": "application/x-www-form-urlencoded"},
                    body: query,
                },
                (err, resp, body) => {
                    if (err) {
                        reject(`<h4>Incorrect Insta URL</h4>`);
                        return;
                    }

                    let instaEle = new JSSoup(body);
                    if (!instaEle.find("a")) {
                        reject(`<h4>Could not find link in response</h4>
                                <p>with Query <pre>${query}</pre></p>
                               <pre> ${body} </pre>`);
                        return;
                    }
                    let link = instaEle.find("a").attrs["href"];
                    let nxtApi = `<div>
                                    <h3>Click on image</h3>
                                    <a href="?action=READ_IMAGE&url=${encodeURIComponent(link)}">
                                        <img alt="If not loaded report" src="${link}"/>
                                    </a>
                                </div>`;
                    resolve(`<div>${nxtApi}</div>`);
                }
            );
        }, (_err) => {
            reject(_err);
        });
    });
};

exports.getDownloadGramAPIKeys = getDownloadGramAPIKeys;
exports.getImgLink = getImgLink;
