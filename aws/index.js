const instaCrawler = require("./insta_crawler");
const imageReader = require("./image_reader");
const request = require("request");

const versionHttp = () => {
    return new Promise((resolve, reject) => {
        console.log("Starting request");
        request.get(
            'https://data.services.jetbrains.com/products/releases?code=WS&latest=true&type=release',
            (err, resp, body) => {
                if (err) {
                    let content = JSON.stringify(err, null, 4);
                    reject(`<pre>${content}</pre>`);
                } else {
                    let content = JSON.stringify(body, null, 4);
                    resolve(`<div>
                                <h4>Incorrect Action Type</h4>
                                <a href="?action=GET_IMAGE_URL&url=${encodeURIComponent("https://www.instagram.com/p/Br9zsAJF9UF/")}">
                                    Go here If you are super lazy or just testing things
                                </a>
                                <h4>TEST API resp</h4>
                                <pre id="content"> ${content} </pre>
                            </div>`);
                }
            }
        );
    });
};

const API = async (req) => {

    let {action, url} = req["queryStringParameters"] ? req["queryStringParameters"] : {};

    let promise = "";
    switch (action) {
        case "GET_IMAGE_URL":
            promise = await instaCrawler.getImgLink(decodeURIComponent(url));
            break;
        case "READ_IMAGE":
            promise = await imageReader.readImageBrowser(decodeURIComponent(url));
            break;
        case "TRANSLATE_TEXT":
            promise = await imageReader.readImageBrowser(decodeURIComponent(url));
            break;
        default:
            promise = await versionHttp();
    }

    return {
        statusCode: 200,
        headers: {"Content-Type": "text/html"},
        body: promise,
    };
};

exports.versionHttp = versionHttp;
exports.API = API;
