const instaCrawler = require("./insta_crawler");
const imageReader = require("./image_reader");
const app = require("./index");

function test1() {
    app.versionHttp().then((resp) => {
        // console.log(resp);
        console.log("Test1", resp.slice(0, 20) + "...");
    }).catch((err) => console.error(err));
}

function test2() {
    instaCrawler.getDownloadGramAPIKeys((id, key) => {
        console.log("API ID = ", id);
        console.log("API Key = ", key);
    });
}

function test3() {
    instaCrawler.getImgLink("https://www.instagram.com/p/Br9zsAJF9UF/")
        .then((resp) => {
            console.log("Test 3", resp);
        })
        .catch((err) => {
            console.error("Test 3", err);
        });
}

const imageToTranslate = "https://scontent-lga3-1.cdninstagram.com/vp/85d49406efba975f493d1d6b8c8610b3/5CB325F8/" +
    "t51.2885-15/e35/46533316_1124651957712328_2866547375857308817_n.jpg?_nc_ht=scontent-lga3-1.cdninstagram.com&dl=1";

function test4(done) {
    imageReader.readImageNode(imageToTranslate,
        (res) => {
            console.log("Test4", res.text, res.confidence);
            done(true);
        }, (err) => {
            console.error(err);
            done(false);
        });
}

function test5() {
    imageReader.readImageBrowser(imageToTranslate)
        .then((res) => {
            console.log("Test5", res);
            // console.log("Test4", res.slice(0, 20) + "...");
        })
        .catch((err) => console.error(err));
}

test1();
test2();
test3();
test4((passed) => {
    if (passed)
        test5();
});

/* Cloud Location
s3://redit-cloud-store/aws.zip
*/
