const fs = require("fs");
const request = require("request").defaults({encoding: null});
const Tesseract = require('tesseract.js');


const readImageNode = (url, callback, errClb) => {
    let tmpFilePath = "/tmp/image_stream";
    request(url).pipe(fs.createWriteStream(tmpFilePath))
        .on('close', () => {
            let image = fs.readFileSync(tmpFilePath);
            Tesseract.recognize(image, {lang: 'deu'})
                .progress(function (p) {
                    console.log('Progress,', p, p["status"], p["progress"]);
                })
                .then(function (result) {
                    console.log("Found Text [" + result.text + "] with accuracy [" + result.confidence + "]");
                    callback(result);
                });
        });
};

const readImageBrowser = (url) => {
    return new Promise((resolve, reject) => {
        request.get(url, (err, resp, body) => {
            let data = "data:" + resp.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');
            if (err)
                reject(err);
            else
                resolve(`
                        <h3 id="heading">
                            <span id="status">Loading</span>
                            <span>In Progress...</span>
                        </h3>
                        <h6>
                            Progress <span id="progress">0</span>%
                        </h6>

                        <div>
                            <img alt="asd"
                                 id="insta_direct_img"
                                 src='${data}'/>
                            <form action="" id="result_form" method="get" style="display: none; margin: 10px;">
                                <div>
                                    <label for="result" id="text_in_image" style="margin: 10px;">Text in image:</label>
                                    <textarea cols="30" id="result" name="imageText" rows="10"></textarea>
                                </div>
                                <div>
                                    <input name="url" type="hidden" value="${url}">
                                    <input name="action" type="hidden" value="TRANSLATE_TEXT">
                                    <input type="submit">
                                </div>
                            </form>
                            <script src='https://cdn.jsdelivr.net/gh/naptha/tesseract.js@v1.0.14/dist/tesseract.min.js'></script>
                            <script>
                                (function () {
                                    let progressEle = document.getElementById("progress");
                                    let headingEle = document.getElementById("heading");
                                    let resultEle = document.getElementById("result");
                                    let statusEle = document.getElementById("status");
                                    let imageEle = document.getElementById("insta_direct_img");
                                    let formEle = document.getElementById("result_form");
                                    Tesseract.recognize(imageEle, {lang: 'deu'})
                                        .progress((p) => {
                                            statusEle.innerText = p.status;
                                            progressEle.innerText = p.progress ? (p.progress * 100) : 0;
                                        }).then((result) => {
                                        headingEle.innerHTML = "Done with accuracy " + result.confidence;
                                        formEle.style.display = "";
                                        resultEle.value = result.text;
                                    });
                                })();
                            </script>
                        </div>
                        `);
        });
    });
};

exports.readImageBrowser = readImageBrowser;
exports.readImageNode = readImageNode;
